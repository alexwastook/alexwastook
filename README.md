- 👋 Hi, I’m @alexwastook
- 👀 I’m interested in ... IT
- 🌱 I’m currently learning ... Java at school and python3 on my free time
- 💞️ I’m looking to collaborate on ... Medium and small projects in Python or Java
- 📫 How to reach me ... Through Gitlab

<!---
alexwastook/alexwastook is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->
